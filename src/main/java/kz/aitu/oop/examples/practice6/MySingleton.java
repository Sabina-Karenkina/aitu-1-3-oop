package kz.aitu.oop.examples.practice6;

public class MySingleton {
    private MySingleton(){}
    private static MySingleton instance = null;
    public String str;
    public static MySingleton getSingleInstance(){
        if (instance == null) { instance =  new MySingleton();}
        return instance;
    }
}
