package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class NewSpecialString {
    private int[] a;
    public NewSpecialString(int[] values, boolean is) {
        for(int i = 0; i < values.length; i++){
            for(int j = i+1; j < values.length; i++){
                if(values[i] == values[j] ) values[j] = values[j-1];
            }
        }
        a = values;
    }
    public NewSpecialString(int[] values){ a = values; }
    public int length() { return a.length; }

    public int valueAt(int position) {
        if (position > a.length) { return -1;}
        return a[position];
    }

    public boolean contains(int value) {
        for ( int i : a) {
            if (value == i) return true;
        }
        return false;
    }


    public int count(int value) {
        int count = 0;
        for(int i : a) {
            if (value == i) count++;
        }
        return count;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
       NewSpecialString o = new NewSpecialString(arr);
        o.count(7);
    }
}
