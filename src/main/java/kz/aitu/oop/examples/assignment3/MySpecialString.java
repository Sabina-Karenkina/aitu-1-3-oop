package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class MySpecialString {
    private int[] a;
    public MySpecialString(int[] values){
        for(int i = 0; i < values.length-1; i++){
            for(int j = i + 1; j < values.length-1; j++){
                if(values[i] == values[j] ) values[j] = values[j-1];
            }
        }
      a = values;
    }
    public int length() { return a.length; }

    public int valueAt(int position) {
        if (position > a.length) { return -1;}
        return a[position];
    }

    public boolean contains(int value) {
        for ( int i : a) {
            if (value == i) return true;
        }
        return false;
    }


    public int count(int value) {
        int count = 0;
        for(int i : a) {
            if (value == i) count++;
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        MySpecialString o = new MySpecialString(arr);
        o.count(7);
    }
}
