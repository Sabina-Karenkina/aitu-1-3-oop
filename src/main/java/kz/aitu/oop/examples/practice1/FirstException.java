package kz.aitu.oop.examples.practice1;

import java.io.FileNotFoundException;

public class FirstException {

   public FirstException(String message) {
        message = "this is bound to execute";
        System.out.println(message);
    }

    public static void main(String[] args) throws Exception {
        try {
            throw new FileNotFoundException();
        }
        catch (FileNotFoundException e) {
            throw new Exception("File not found");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("i was there hehe");
        }

    }
}
