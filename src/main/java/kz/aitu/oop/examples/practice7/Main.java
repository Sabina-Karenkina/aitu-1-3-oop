package kz.aitu.oop.examples.practice7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory = new FoodFactory();
        Scanner sc = new Scanner(System.in);
        Food food = foodFactory.getFood(sc.nextLine());
        System.out.println("The factory returned " + food.getClass());
        System.out.println(food.getType());
    }
}
