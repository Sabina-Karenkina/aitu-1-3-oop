package kz.aitu.oop.examples.practice3;

public class JavaDeveloper extends Worker implements BackEndDeveloper {
    public JavaDeveloper(String name) {
        super(name);
    }

    @Override
    public void developBack() {
        System.out.println("hello,my name is " + getName() + ",I'm developing Back end with Java");
    }

    @Override
    public void develop() {
        developBack();
    }

    @Override
    public void work() {
        develop();
    }
}
