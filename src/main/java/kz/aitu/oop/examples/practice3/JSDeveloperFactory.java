package kz.aitu.oop.examples.practice3;

public class JSDeveloperFactory implements DeveloperFactory{
    @Override
    public Developer createDeveloper() {
        return new JSDeveloper("ken");
    }
}
