package kz.aitu.oop.examples.practice3;

import java.util.ArrayList;
import java.util.List;

public class Company {

    static DeveloperFactory createBySpecialty(String specialty) {
        if (specialty.equalsIgnoreCase("java")) {
            return new JavaDeveloperFactory();
        } else if (specialty.equalsIgnoreCase("javascript")) {
            return new JSDeveloperFactory();
        } else if (specialty.equalsIgnoreCase("fullstack")) {
            return new FullStackDeveloperFactory();
        } else {
            throw new RuntimeException("there are no "+ specialty + " developers in our company");
        }
    }


    public static void main(String[] args) {
        DeveloperFactory developerFactory = createBySpecialty("java");
        Developer developer = developerFactory.createDeveloper();
        developer.develop();

        DeveloperFactory developerFactory1 = createBySpecialty("javascript");
        Developer developer1 = developerFactory1.createDeveloper();
        developer1.develop();

        DeveloperFactory developerFactory2 = createBySpecialty("fullstack");
        Developer developer2 = developerFactory2.createDeveloper();
        developer2.develop();

        DeveloperFactory developerFactory3 = createBySpecialty("c++");
        Developer developer3 = developerFactory3.createDeveloper();
        developer3.develop();
    }
}
