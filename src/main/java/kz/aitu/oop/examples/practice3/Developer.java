package kz.aitu.oop.examples.practice3;

public interface Developer extends Person {
    void develop();
}
