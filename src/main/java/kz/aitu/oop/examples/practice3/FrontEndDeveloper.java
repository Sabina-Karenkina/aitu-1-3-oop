package kz.aitu.oop.examples.practice3;

public interface FrontEndDeveloper extends Developer{
    void developFront();
}
