package kz.aitu.oop.examples.practice3;

public class FullStackDeveloper extends Worker implements FrontEndDeveloper, BackEndDeveloper{

    public FullStackDeveloper(String name) {
        super(name);
    }
    @Override
    public void develop() {
        System.out.println("hello, my name is "+ getName());
        developBack();
        developFront();
    }

    @Override
    public void developBack() {
        System.out.println("I'm developing back end with PHP!");
    }

    @Override
    public void developFront() {
        System.out.println("I'm developing front end with CSS");
    }


    @Override
    public void work() {
        develop();
    }
}
