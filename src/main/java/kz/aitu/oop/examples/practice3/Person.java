package kz.aitu.oop.examples.practice3;

public interface Person {
    String getName();
    void setName(String name);
}
