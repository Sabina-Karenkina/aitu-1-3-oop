package kz.aitu.oop.examples.practice3;

public class FullStackDeveloperFactory implements DeveloperFactory {
    @Override
    public Developer createDeveloper() {
        return new FullStackDeveloper("nicole");
    }
}
