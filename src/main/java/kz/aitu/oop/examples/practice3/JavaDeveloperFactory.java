package kz.aitu.oop.examples.practice3;

public class JavaDeveloperFactory implements DeveloperFactory{
    @Override
    public Developer createDeveloper() {
        return new JavaDeveloper("jon");
    }
}
