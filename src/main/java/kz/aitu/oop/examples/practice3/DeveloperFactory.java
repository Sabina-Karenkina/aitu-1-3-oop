package kz.aitu.oop.examples.practice3;

public interface DeveloperFactory {
    Developer createDeveloper();
}
