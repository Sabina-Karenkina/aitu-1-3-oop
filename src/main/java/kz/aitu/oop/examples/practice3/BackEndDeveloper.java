package kz.aitu.oop.examples.practice3;

public interface BackEndDeveloper extends Developer{
    void developBack();
}
