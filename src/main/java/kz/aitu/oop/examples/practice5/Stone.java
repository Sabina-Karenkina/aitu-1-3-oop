package kz.aitu.oop.examples.practice5;
import java.util.ArrayList;
import java.util.List;

public class Stone{
        protected double price = 0;
        protected double weight = 0;
        private static double totalPreciousPrice = 0;
        private static double totalPreciousWeight = 0;
        private static double totalSemiPreciousPrice = 0;
        private static double totalSemiPreciousWeight = 0;

        public Stone(double price, double weight) {
            this.price = price;
            this.weight = weight;
            totalPreciousPrice += this.price;
            totalPreciousWeight += this.weight;
            totalSemiPreciousPrice += this.price;
            totalSemiPreciousWeight += this.weight;
        }

        public static double getTotalPreciousPrice() { return totalPreciousPrice; }

        public static double getTotalPreciousWeight() { return totalPreciousWeight; }

        public static double getTotalSemiPreciousPrice() { return totalSemiPreciousPrice; }

        public static double getTotalSemiPreciousWeight() { return totalSemiPreciousWeight; }

        public static void main(String[] args) {
            List<Stone> necklace = new ArrayList<Stone>();
            for (double i = 0; i < 10; i += 2) {
                necklace.add(new PreciousStone(i * 1000, i + 0.1, "diamond" + i));
            }
            for (double i = 1; i < 10; i += 3) {
                necklace.add(new SemiPreciousStone(i * 500, i + 0.15, "agate" + i));
            }

            System.out.println("Precious necklace: Total weight:" + getTotalPreciousWeight() + ";Total price:" + getTotalPreciousPrice());
            System.out.println("SemiPrecious necklace: Total weight:" + getTotalSemiPreciousWeight() + ";Total price:" + getTotalSemiPreciousPrice());
        }
}
