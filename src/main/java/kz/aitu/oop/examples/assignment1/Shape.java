package kz.aitu.oop.examples.assignment1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Shape {
    Point begin;

    public Shape() {
    }

    private class Point {
        double x;
        double y;
        Point next;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
            this.next = null;
        }
    }

    public void addPoint(double x, double y) {
        Point point = new Point(x, y);
        if (begin == null) {
            begin = point;
        } else {
            Point tmp = begin;
            while (tmp != null) {
                tmp = tmp.next;
            }
            tmp.next = point;
        }
    }

    public void getPoints() {
        Point point = begin;
        while (point.next != null) {
            System.out.println("(" + point.x + point.y + ")");
            point = point.next;
        }
        System.out.println("(" + point.x + point.y + ")");
    }

    public double calculatePerimeter() {
        Point point = begin;
        double per = 0;
        while (point.next != null) {
            per = per + Math.sqrt(Math.pow(point.next.x - point.x, 2) + Math.pow(point.next.y - point.y, 2));
            point = point.next;
        }
        per = per + Math.sqrt(Math.pow(begin.next.x - begin.x, 2) + Math.pow(begin.next.y - begin.y, 2));
        return per;
    }

    public double longestSide() {
        Point point = begin;
        double l = 0;
        while (point.next != null) {
            if (l <= Math.sqrt(Math.pow(point.next.x - point.x, 2) + Math.pow(point.next.y - point.y, 2))) {
                l = Math.sqrt(Math.pow(point.next.x - point.x, 2) + Math.pow(point.next.y - point.y, 2));
            }
            point = point.next;
        }
        if (l <= Math.sqrt(Math.pow(begin.next.x - begin.x, 2) + Math.pow(begin.next.y - begin.y, 2))) {
            l = Math.sqrt(Math.pow(begin.next.x - begin.x, 2) + Math.pow(begin.next.y - begin.y, 2));
        }
        return l;
    }

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("//Users//User-PC//Desktop//input1.txt");
        Scanner sc = new Scanner(file);

        String result = "";
        int count = 0;
        while (sc.hasNextLine()) {
            result += sc.nextLine() + "\n";
        }
	count++;
    }
}

