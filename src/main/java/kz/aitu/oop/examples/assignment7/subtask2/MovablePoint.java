package kz.aitu.oop.examples.assignment7.subtask2;

import kz.aitu.oop.examples.assignment7.subtask2.Movable;

public class MovablePoint implements Movable {
     int x, y, xSpeed, ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public String toString() { return "Point (" + x + "," + y + ") " + "with speed of x: " + xSpeed + " and speed of y: " + ySpeed; }

    @Override
    public void moveUp() { y -= ySpeed; }

    @Override
    public void moveDown() { y += ySpeed; }

    @Override
    public void moveLeft() { x-= xSpeed; }

    @Override
    public void moveRight() { x += xSpeed; }
}
