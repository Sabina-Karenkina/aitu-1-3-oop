package kz.aitu.oop.examples.assignment7.subtask1;

import kz.aitu.oop.examples.assignment7.subtask1.Rectangle;

public class Square extends Rectangle {
    public Square() { super(); }

    public Square(double side) { super(side, side); }

    public Square(double side, String color, boolean filled) {
        super(side, side);
    }

    public double getSide() { return super.getWidth(); }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }

    public void setLength(double length) {
        super.setWidth(length);
        super.setLength(length);
    }

    @Override
    public String toString() {
        return "Square[" + super.toString() + "]";
    }
}
