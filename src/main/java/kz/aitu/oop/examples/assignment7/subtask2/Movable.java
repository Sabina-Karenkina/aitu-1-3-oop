package kz.aitu.oop.examples.assignment7.subtask2;

public interface Movable {
    abstract void moveUp();
    abstract void moveDown();
    abstract void moveLeft();
    abstract void moveRight();
}
