package kz.aitu.oop.examples.assignment7.subtask3;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) { super(radius); }

    @Override
    public String toString(){ return super.toString(); }

    @Override
    public double resize(int percent){  return radius = (radius * percent)/100; }

}
