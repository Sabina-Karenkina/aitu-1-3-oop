package kz.aitu.oop.examples.assignment7.subtask2;

import kz.aitu.oop.examples.assignment7.subtask2.Movable;
import kz.aitu.oop.examples.assignment7.subtask2.MovablePoint;

public class MovableCircle implements Movable {
  private int radius;
  MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed,int radius) {
      this.center = new MovablePoint(x, y, xSpeed, ySpeed);
      this.radius = radius;
    }

  @Override
  public String toString() { return center.toString(); }

  @Override
    public void moveUp() { center.y -= center.ySpeed; }

    @Override
    public void moveDown() { center.y += center.ySpeed; }

    @Override
    public void moveLeft() { center.x -= center.xSpeed; }

    @Override
    public void moveRight() { center.x += center.xSpeed; }

}
