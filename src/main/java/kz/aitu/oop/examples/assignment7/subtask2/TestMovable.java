package kz.aitu.oop.examples.assignment7.subtask2;

public class TestMovable {
    public static void main(String[] args) {
        Movable point1 = new MovablePoint(1, 2, 2, 3);
        System.out.println(point1);
        point1.moveLeft();
        System.out.println(point1);

        Movable point2 = new MovableCircle(5, 5, 3, 4, 9);
        System.out.println(point2);
        point2.moveRight();
        System.out.println(point2);
    }
}
