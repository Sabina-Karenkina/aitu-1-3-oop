package kz.aitu.oop.examples.assignment7.subtask3;

public class TestResCircle {
    public static void main(String[] args) {
        ResizableCircle c2 = new ResizableCircle(6);
        System.out.println(c2.toString());
        System.out.println(c2.getPerimeter());
        System.out.println(c2.getArea());
        c2.resize(50);
        System.out.println(c2);
        System.out.println(c2.getPerimeter());
        System.out.println(c2.getArea());
    }
}
