package kz.aitu.oop.examples.assignment7.subtask1;

import kz.aitu.oop.examples.assignment7.subtask1.Shape;

public class Circle extends Shape {
    protected double radius;

    public Circle() {
        super();
        this.radius = 1.0;
    }

    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() { return radius; }

    public void setRadius(double radius) { this.radius = radius; }

    @Override
    public double getArea() { return radius * radius * Math.PI; }

    @Override
    public double getPerimeter() { return  2.0 * Math.PI * radius; }

    @Override
    public String toString() { return "Circle[" + super.toString() + " , radius =" + radius + "]"; }
}
