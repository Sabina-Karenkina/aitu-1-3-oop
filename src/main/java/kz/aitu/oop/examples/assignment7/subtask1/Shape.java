package kz.aitu.oop.examples.assignment7.subtask1;

abstract public class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
        this.color = "red";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() { return this.color; }

    public void setColor() { this.color = color; }

    public boolean isFilled() { return this.filled; }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract public double getArea();

    abstract public double getPerimeter();

    public String toString() { return "Shape[color = " + color + ",filled = " + filled + "]"; }

}
