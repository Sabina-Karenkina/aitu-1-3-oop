package kz.aitu.oop.examples.assignment7.subtask3;

public class Circle implements GeometricObject {
    protected double radius;

    public Circle() { this.radius = 1.0; }

    public Circle(double radius) { this.radius = radius; }

    @Override
    public String toString() { return " radius = " + radius; }

    @Override
    public double getPerimeter() { return 2 * Math.PI * radius; }

    @Override
    public double getArea() { return Math.PI * Math.pow(radius , 2); }
}
