package kz.aitu.oop.examples.jdbc;
import kz.aitu.oop.entity.Student;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class Main {

    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
        String username = "postgres";
        String password = "";

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from student");

            List<Student> studentList = new ArrayList<>();
            while(resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setAge(resultSet.getInt("age"));
                student.setPoint(resultSet.getDouble("point"));

                System.out.println(student);
                studentList.add(student);
            }

            resultSet.close();
            statement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

