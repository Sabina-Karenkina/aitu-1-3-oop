package kz.aitu.oop.examples.practice4;

public class FishBuilder implements AquariumBuilder{
    private Aquarium fish1 = new Aquarium();

    @Override
    public void buildName() {
        fish1.setName("fuju");
    }

    @Override
    public void buildPrice() {
    fish1.setPrice(10);
    }

    @Override
    public void buildWeight() {
    fish1.setWeight(2);
    }
}
