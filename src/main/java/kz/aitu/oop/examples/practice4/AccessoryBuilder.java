package kz.aitu.oop.examples.practice4;

public class AccessoryBuilder implements AquariumBuilder{
    private Aquarium acc1 = new Aquarium();
    @Override
    public void buildName() {
        acc1.setName("backlight");
    }

    @Override
    public void buildPrice() {
    acc1.setPrice(60);
    }

    @Override
    public void buildWeight() {
    acc1.setWeight(10);
    }
}
