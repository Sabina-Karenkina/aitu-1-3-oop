package kz.aitu.oop.examples.practice4;

public class ReptileBuilder implements AquariumBuilder{
    private Aquarium reptile1 = new Aquarium();
    @Override
    public void buildName() {
        reptile1.setName("snake");
    }

    @Override
    public void buildPrice() {
    reptile1.setPrice(100);
    }

    @Override
    public void buildWeight() {
    reptile1.setWeight(4);
    }
}
