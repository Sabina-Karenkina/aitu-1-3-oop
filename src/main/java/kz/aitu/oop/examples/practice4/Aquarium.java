package kz.aitu.oop.examples.practice4;

public class Aquarium {

    private String name;
    private double price = 0;
    private double weight = 0;
    private double totalPrice = 0;
    private double totalWeight = 0;

//    public Aquarium(double price, double weight) {
//        this.price = price;
//        this.weight = weight;
//        totalPrice += this.price;
//        totalWeight += this.weight;
//    }
//
//    public Aquarium(double weight) {
//        this.weight = weight;
//        totalWeight += this.weight;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {
        totalPrice += this.price;
        return totalPrice;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public double getTotalWeight() {
        totalWeight += this.weight;
        return totalWeight;
    }

    public static void main(String[] args) {
        AquariumBuilder aquariumBuilder = new FishBuilder();

    }
}
