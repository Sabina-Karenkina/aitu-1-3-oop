package kz.aitu.oop.examples.practice4;

public interface AquariumBuilder {
    public void buildName();
    public void buildPrice();
    public void buildWeight();

}
