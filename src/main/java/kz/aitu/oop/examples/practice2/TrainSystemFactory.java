package kz.aitu.oop.examples.practice2;

public interface TrainSystemFactory {
    Locomotive getLocomotive();
    Carriage getCarriage();
}
