package kz.aitu.oop.examples.practice2;

public class TrainFactory implements TrainSystemFactory{
    @Override
    public Locomotive getLocomotive() {
        return new LocomotiveTrain("lc-235",75.1);
    }

    @Override
    public Carriage getCarriage() {
        return new CarriageTrain("lc-236",120);
    }
}
