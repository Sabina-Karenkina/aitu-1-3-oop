package kz.aitu.oop.examples.practice2;

public interface TrainConstruction extends TrainSystem{
    void construct();
}
