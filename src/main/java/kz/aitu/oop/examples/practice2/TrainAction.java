package kz.aitu.oop.examples.practice2;

public interface TrainAction extends TrainSystem{
    void ride();
}
