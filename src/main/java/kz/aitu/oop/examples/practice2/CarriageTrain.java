package kz.aitu.oop.examples.practice2;

public class CarriageTrain extends RidingTrain implements Carriage {

    private int numberOfPassengers;

    public CarriageTrain(String name,int numberOfPassengers ) {
        super(name);
        this.numberOfPassengers = numberOfPassengers;
    }

    public int getNumberOfPassengers(){
        return numberOfPassengers;
    }
    public void setNumberOfPassengers(int numberOfPassengers){
        this.numberOfPassengers = numberOfPassengers;
    }



    @Override
    public void ride() {
        construct();
    }

    @Override
    public void construct() {
        constructBack();
    }

    @Override
    public void constructBack() {
        System.out.println(getName() + "-this part rides in back and can hold 'till " + getNumberOfPassengers()+" passengers");
    }
}
