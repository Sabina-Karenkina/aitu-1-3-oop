package kz.aitu.oop.examples.practice2;

public interface TrainSystem {
    String getName();
    void setName(String name);
}
