package kz.aitu.oop.examples.practice2;

public class Train {
    public static void main(String[] args) {
        TrainSystemFactory trainSystemFactory = new TrainFactory();
        Locomotive locomotive = trainSystemFactory.getLocomotive();
        Carriage carriage = trainSystemFactory.getCarriage();
        System.out.println("Train ready to go!");
        locomotive.constructFront();
        carriage.constructBack();
    }
}
