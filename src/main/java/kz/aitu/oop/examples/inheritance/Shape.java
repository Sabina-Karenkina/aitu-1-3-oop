package kz.aitu.oop.examples.inheritance;

public class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        this.color = "red";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() { return this.color; }

    public void setColor() { this.color = color; }

    public boolean isFilled() { return this.filled; }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() { return "Shape[color = " + color + ",filled = " + filled + "]"; }

    public static void main(String[] args) {
        Shape s = new Shape();
        System.out.println(s.getColor());
        System.out.println(s.isFilled());
        System.out.println(s.toString());
    }







}
