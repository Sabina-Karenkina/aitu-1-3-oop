package kz.aitu.oop.examples.inheritance;

public class Square extends Rectangle{
    public Square() { super(); }

    public Square(double side) { super(side, side); }

    public Square(double side, String color, boolean filled) {
        super(side, side);
    }

    public double getSide() { return super.getWidth(); }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }

    public void setLength(double length) {
        super.setWidth(length);
        super.setLength(length);
    }

    @Override
    public double getArea() {
        return super.getWidth() * super.getWidth();
    }

    @Override
    public double getPerimeter() {
        return 4 * super.getWidth();
    }

    @Override
    public String toString() {
        return "Square[" + super.toString() + "]";
    }

    public static void main(String[] args) {
        Square square = new Square();
        square.setSide(5);
        System.out.println(square.toString());
        System.out.println(square.getPerimeter());
        System.out.println(square.getArea());
    }
}
