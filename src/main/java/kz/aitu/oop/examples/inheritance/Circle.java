package kz.aitu.oop.examples.inheritance;

public class Circle extends Shape {
    private double radius;

    public Circle() {
        super();
        this.radius = 1.0;
    }

    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() { return radius; }

    public void setRadius(double radius) { this.radius = radius; }

    public double getArea() { return radius * radius * Math.PI; }

    public double getPerimeter() { return  2.0 * Math.PI * radius; }

    @Override
    public String toString() {
        return "Circle[" + super.toString() + " , radius =" + radius + "]";
    }

    public static void main(String[] args) {
        Circle c = new Circle();
        c.setRadius(6);
        System.out.println(c.toString());
        System.out.println(c.getPerimeter());
        System.out.println(c.getArea());
    }
}
