package kz.aitu.oop.practice2;

import kz.aitu.oop.practice3.Employee;

import java.util.ArrayList;
import java.util.List;

public class Train {
    private List<TrainAction> activeTrains = new ArrayList<>();

    public void addTrain(TrainAction active) {
        activeTrains.add(active);
    }

    public void startRide() {
        activeTrains.forEach(TrainAction::ride);
    }

    public static void main(String[] args) {
        Train set1 = new Train();
        set1.addTrain(new LocomotiveTrain("tw225", 72.6));
        set1.addTrain(new CarriageTrain("tw226", 120));
        set1.startRide();
    }


}
