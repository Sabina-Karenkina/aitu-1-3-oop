package kz.aitu.oop.practice2;

public abstract class RidingTrain implements TrainAction{
    private String name;

    public RidingTrain(String name) {
        setName(name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
    this.name = name;
    }


}
