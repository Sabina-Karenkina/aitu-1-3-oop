package kz.aitu.oop.practice2;

public interface Locomotive extends TrainConstruction {
    void constructFront();
}
