package kz.aitu.oop.practice2;

public interface TrainConstruction extends TrainSystem{
    void construct();
}
