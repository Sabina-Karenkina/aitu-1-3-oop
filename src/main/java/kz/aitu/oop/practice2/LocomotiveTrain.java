package kz.aitu.oop.practice2;

public class LocomotiveTrain extends RidingTrain implements Locomotive{
    private double carryingCapacity;

    public LocomotiveTrain(String name,double carryingCapacity) {
        super(name);
        this.carryingCapacity = carryingCapacity;
    }

    public double getCarryingCapacity(){
        return carryingCapacity;
    }

    public void setCarryingCapacity(double carryingCapacity){
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public void constructFront() {
        System.out.println(getName() + "-this part of train rides in front and its carrying capacity is " + getCarryingCapacity());
    }

    @Override
    public void ride() {
    construct();
    }

    @Override
    public void construct() {
    constructFront();
    }
}
