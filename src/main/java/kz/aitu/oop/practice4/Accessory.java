package kz.aitu.oop.practice4;

public class Accessory extends Aquarium {
    String nameOfAccessory;

    public Accessory(double price, double weight, String nameOfAccessory) {
        super(price, weight);
    }

    public Accessory(double price, double weight) {
        super(price, weight);
    }

    public Accessory(double weight) {
        super(weight);
    }

    public Accessory(double weight, String nameOfAccessory) {
        super(weight);
    }
}
