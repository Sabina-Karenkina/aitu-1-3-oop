package kz.aitu.oop.practice4;

public class Aquarium {
        protected double price = 0;
        protected double weight = 0;
        private static double totalPrice = 0;
        private static double totalWeight = 0;


        public Aquarium(double price, double weight) {
            this.price = price;
            this.weight = weight;
            totalPrice += this.price;
            totalWeight += this.weight;
        }

        public Aquarium(double weight) {
            this.weight = weight;
            totalWeight += this.weight;
        }

        public static double getTotalPrice() { return totalPrice; }

        public static double getTotalWeight() { return totalWeight; }

        public static void main(String[] args) {

            Aquarium ob1 = new Aquarium(2, 5);
            Aquarium ob2 = new Aquarium(5);

            Fish fish1 = new Fish(2, 5, "Shark");
            Fish fish2 = new Fish(2, 5);
            Fish fish3 = new Fish(2,"Flyfish");
            Fish fish4 = new Fish(5);

            Reptile reptile1 = new Reptile(5,3,"Snake");
            Reptile reptile2 = new Reptile(5,2);
            Reptile reptile3 = new Reptile(5,"Crocodile");
            Reptile reptile4 = new Reptile(5);

            Accessory accessory1 = new Accessory(35.5, 10, "Backlight");
            Accessory accessory2 = new Accessory(24.5, 2);
            Accessory accessory3 = new Accessory(20, 5, "Plants");
            Accessory accessory4 = new Accessory(2);

            System.out.println("Your total price: " + getTotalPrice() + " and total height: " + getTotalWeight());
        }
    }
