package kz.aitu.oop.practice4;

    public class Fish extends Aquarium {
        String nameOfFish;

        public Fish(double price, double weight, String nameOfFish) {
            super(price, weight);
        }

        public Fish(double price, double weight) {
            super(price, weight);
        }

        public Fish(double weight) {
            super(weight);
        }

        public Fish(double weight, String nameOfFish) {
            super(weight);
        }
    }