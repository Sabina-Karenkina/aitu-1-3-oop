package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String result = "";
        for (Student student : studentFileRepository.getStudents()) {
            if ( student.getGroup().equals(group))
                result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String result = "";


        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
       ArrayList<String> top5 = new ArrayList<>();
        Student max = new Student();
        max.setPoint(0);
        for (Student student : studentFileRepository.getStudents()) {
            if ( student.getPoint() > max.getPoint())
                max = student;
        }
        for (Student student : studentFileRepository.getStudents()) {
            if ( student.getPoint() == max.getPoint())
                top5.add(student.getName());
        }
            return ResponseEntity.ok(top5);
    }
}
