package kz.aitu.oop.practice3;

public abstract class Worker implements Employee{
    private String name;

    public Worker(String name) { setName(name); }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
    this.name = name;
    }


}
