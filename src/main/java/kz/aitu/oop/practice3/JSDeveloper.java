package kz.aitu.oop.practice3;

public class JSDeveloper extends Worker implements FrontEndDeveloper{
    public JSDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        developFront();
    }

    @Override
    public void work() {
    develop();
    }

    @Override
    public void developFront() {
        System.out.println("hello, my name is " + getName()+", I'm developing Front with Javascript");
    }
}
