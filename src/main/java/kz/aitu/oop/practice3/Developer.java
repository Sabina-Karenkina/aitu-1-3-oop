package kz.aitu.oop.practice3;

public interface Developer extends Person{
    void develop();
}
