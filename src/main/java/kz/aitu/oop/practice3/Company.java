package kz.aitu.oop.practice3;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees = new ArrayList<>();

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void startWork() {
        employees.forEach(e -> e.work());
    }

    public static void main(String[] args) {
    Company c = new Company();
    c.addEmployee(new FullStackDeveloper("sam"));
    c.addEmployee(new JavaDeveloper("john"));
    c.addEmployee(new JSDeveloper("troy"));
    c.startWork();
    }
}
