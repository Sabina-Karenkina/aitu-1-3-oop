package kz.aitu.oop.practice3;

public interface FrontEndDeveloper extends Developer{
    void developFront();
}
